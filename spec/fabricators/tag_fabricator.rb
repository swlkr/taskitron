Fabricator(:tag) do
  name 'test'
end

Fabricator(:invalid_tag, from: :tag) do
  name ''
end