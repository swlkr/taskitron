Fabricator(:timer) do
  started_at { Time.now }
end

Fabricator(:completed_timer, from: :timer) do
  started_at { 1.minute.ago }
  stopped_at { Time.now }
end

Fabricator(:completed_timer_one_day_from_now, from: :timer) do
  started_at { 1.day.from_now.beginning_of_day }
  stopped_at { 1.day.from_now.beginning_of_day + 10.seconds }
end