Fabricator(:task) do
  name 'whatever'
end

Fabricator(:invalid_task, from: :task) do
  name ''
end

Fabricator(:task_with_tags, from: :task) do
  name '#front Im a #middle task and I have some tags #back #back2'
end