require "spec_helper"

describe TasksController do
  login_user

  describe "GET #index" do
    let(:task) { Fabricate(:task, user: subject.current_user) }
    it "populates an array of tasks based on the currently logged in user" do
      get :index
      assigns(:tasks).should eq([task])
    end
    it "loads up a new task in memory" do
      get :index
      assigns(:task).should_not eq(nil)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new task in the database" do
        expect{ post :create, task: Fabricate.attributes_for(:task) }.to change(Task, :count).by(1)
      end

      it "starts the new task" do
        post :create, task: Fabricate.attributes_for(:task)
        Task.last.completed?.should be_false
      end

      it "redirects to the task show page" do
        post :create, task: Fabricate.attributes_for(:task)
        response.should redirect_to authenticated_root_path
      end
    end

    context "with invalid attributes" do
      it "does not save the new task in the database" do
        expect{ post :create, task: Fabricate.attributes_for(:invalid_task) }.to_not change(Task, :count)
      end
      it "re-renders the home page" do
        post :create, task: Fabricate.attributes_for(:invalid_task)
        response.should render_template :index
      end
    end
  end

  describe "POST #destroy" do
    let!(:task) { Fabricate(:task, user_id: subject.current_user.id) }

    it "deletes the task" do
      expect{
        delete :destroy, id: task
      }.to change(Task, :count).by(-1)
    end

    it "redirects to the tasks#index" do
      delete :destroy, id: task
      response.should redirect_to authenticated_root_path
    end
  end

  describe "POST #stop" do
    let!(:task) { Fabricate(:task, user: subject.current_user) }
    let!(:timer) { Fabricate(:timer, task: task) }

    it "stops the task" do
      post :stop, id: task
      task.completed?.should be_true
    end

    it "redirects to tasks#index" do
      post :stop, id: task
      response.should redirect_to authenticated_root_path
    end
  end

  describe "POST #start" do
    let!(:task) { Fabricate(:task, user: subject.current_user) }

    it "starts the task" do
      post :start, id: task
      task.completed?.should be_false
    end

    it "redirects to tasks#show" do
      post :start, id: task
      response.should redirect_to authenticated_root_path
    end
  end
end
