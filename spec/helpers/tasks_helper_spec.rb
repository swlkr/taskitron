require 'spec_helper'

describe TasksHelper do
  describe '#format_elapsed_time' do
    let(:seconds) { nil }
    subject { format_elapsed_time(seconds) }

    context 'with 0 seconds' do
      let(:seconds) { 0 }
      it 'should print out 0 seconds' do
        expect(subject).to eq('0 seconds')
      end
    end

    context 'with less than an hour' do
      let(:seconds) { 120 }
      it 'should print out 2 minutes, 0 seconds' do
        expect(subject).to eq('2 minutes, 0 seconds')
      end
    end

    context 'with less than a day' do
      let(:seconds) { 3600 }
      it 'should print out 1 hour, 0 minutes, 0 seconds' do
        expect(subject).to eq('1 hour, 0 minutes, 0 seconds')
      end
    end

    context 'with a day' do
      let(:seconds) { 86400 }
      it 'should print out 1 day, 0 hours, 0 minutes, 0 seconds' do
        expect(subject).to eq('1 day, 0 hours, 0 minutes, 0 seconds')
      end
    end
  end
end
