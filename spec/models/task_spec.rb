require 'spec_helper'

describe Task do
  let!(:user) { Fabricate(:user) }
  let!(:task) { Fabricate(:task, user: user) }

  describe '#timer' do
    subject { task.timer }

    context 'with no timer' do
      it 'should return a new timer' do
        expect(subject).to_not be_persisted
      end
    end

    context 'with a completed timer' do
      let!(:timer) { Fabricate(:completed_timer, task: task) }
      it 'should return a new timer' do
        expect(subject).to_not be_persisted
      end
    end

    context 'with an incomplete timer' do
      let!(:timer) { Fabricate(:timer, task: task) }
      it 'should return the current timer' do
        expect(subject).to eq(timer)
      end
    end
  end

  describe '#elapsed_time' do
    context 'with a stopped timer' do
      let!(:timer) { Fabricate(:completed_timer, task: task) }
      subject { task.elapsed_time }
      it 'should return the elapsed time between starting and stopping' do
        expect(subject).to eq(timer.stopped_at - timer.started_at)
      end
    end

    context "with a timer that hasn't been stopped" do
      let!(:timer) { Fabricate(:timer, task: task) }
      subject { task.elapsed_time }
      it 'should return 0' do
        expect(subject).to eq(0)
      end
    end
  end

  describe '#start' do
    subject { task.start }

    it 'should start the task' do
      expect(subject).to be_true
    end
  end

  describe '#stop' do
    let!(:timer) { Fabricate(:timer, task: task) }
    subject { task.stop }

    it 'should stop the task' do
      expect(subject).to be_true
    end
  end

  describe '#completed?' do
    subject { task.completed? }

    context 'with a running timer' do
      let!(:timer) { Fabricate(:timer, task: task) }
      it 'should be false' do
        expect(subject).to be_false
      end
    end

    context 'with a completed timer' do
      let!(:timer) { Fabricate(:completed_timer, task: task) }
      it 'should be true' do
        expect(subject).to be_true
      end
    end
  end

  describe '#parse_tags' do
    let!(:task) { Fabricate(:task_with_tags, user: user) }
    it 'should create some tags' do
      expect { task.parse_tags }.to change(Tag, :count).by(4)
    end
  end
end
