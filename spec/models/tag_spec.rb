require 'spec_helper'

describe Tag do
  let!(:user) { Fabricate(:user) }
  let!(:task) { Fabricate(:task, user: user) }
  let!(:tag) { Fabricate(:tag, task: task) }
  let!(:timer) { Fabricate(:completed_timer, task: task) }
  let!(:timer_a) { Fabricate(:completed_timer, task: task) }

  describe '#elapsed_time' do
    let(:sum) { timer.elapsed_time + timer_a.elapsed_time }
    subject { tag.elapsed_time }

    it 'should return the sum of task elapsed times' do
      expect(subject).to eq(sum)
    end
  end

  describe '#elapsed_time_by_day' do
    let!(:future_task) { Fabricate(:task, user: user, created_at: 1.day.from_now.beginning_of_day) }
    let!(:future_timer) { Fabricate(:completed_timer_one_day_from_now, task: future_task) }
    let!(:future_tag) { Fabricate(:tag, name: 'future_tag', task: future_task) }
    let(:sum_by_day) { future_timer.elapsed_time }
    subject { future_tag.elapsed_time_by_day(1.day.from_now.beginning_of_day) }

    it 'should return the sum of elapsed times for the same day' do
      expect(subject).to eq(sum_by_day)
    end
  end
end
