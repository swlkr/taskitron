require 'spec_helper'

describe Timer do
  let(:timer) { Fabricate.build(:timer) }

  describe "#start" do
    before { timer.start }
    subject { timer.started? }
    it "should start" do
      expect(subject).to be_true
    end
  end

  describe "#stop" do
    before { timer.stop }
    subject { timer.stopped? }
    it "should stop" do
      expect(subject).to be_true
    end
  end

  describe "#elapsed_time" do
    subject { timer.elapsed_time }

    context "with start and stop times" do
      let(:timer) { Fabricate.build(:completed_timer) }
      let(:elapsed_time) { timer.stopped_at - timer.started_at }
      it "should return the elapsed time" do
        expect(subject).to eq(elapsed_time)
      end
    end

    context "with only a start time" do
      it "should return 0" do
        expect(subject).to eq(0)
      end
    end
  end

  describe "#completed?" do
    subject { timer.completed? }

    context "with a start time" do
      it "should be false" do
        expect(subject).to be_false
      end
    end

    context "with a start and stop time" do
      let(:timer) { Fabricate.build(:completed_timer) }
      it "should be true" do
        expect(subject).to be_true
      end
    end
  end
end
