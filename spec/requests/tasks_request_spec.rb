require "spec_helper"

describe "Tasks" do
  before { sign_in_as_a_valid_user }

  describe "GET /" do
    let(:task) { Fabricate(:task, user: subject.current_user.id) }
    it "should display a list of tasks" do
      get tasks_path

      response.status.should eq(200)
    end
  end
end
