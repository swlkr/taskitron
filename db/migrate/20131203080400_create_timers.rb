class CreateTimers < ActiveRecord::Migration
  def change
    create_table :timers do |t|
      t.timestamp :started_at
      t.timestamp :stopped_at
      t.integer :task_id

      t.timestamps
    end
  end
end
