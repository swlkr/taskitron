class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :user_id
      t.string :name
      t.timestamp :started_at
      t.timestamp :stopped_at

      t.timestamps
    end
  end
end
