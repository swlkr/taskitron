class RemoveStartedAtAndStoppedAtFromTasks < ActiveRecord::Migration
  def up
    Task.where('started_at IS NOT NULL').each { |t| t.timers.create(started_at: t.started_at, stopped_at: t.stopped_at) }
    remove_column :tasks, :started_at
    remove_column :tasks, :stopped_at
  end

  def down
    add_column :tasks, :started_at, :timestamp
    add_column :tasks, :stopped_at, :timestamp
  end
end
