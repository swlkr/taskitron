Taskitron
===

A rails app for keeping track of your time

Dependencies
---
- rbenv
- postgresql
- ruby 2.0.0-p247

Get up and running
---
    $ git clone git@github.com:swlkr/taskitron.git
    $ cd taskitron
    $ bundle
    $ rake db:create
    $ rake db:migrate
    $ rake db:seed
    $ bundle exec foreman s

Testing
---
    $ rake
