namespace :tags do
  task parse: :environment do
    Task.select(:name, :id).each do |task|
      tags = task.name.scan(/\B#\S+/).map { |t| { task_id: task.id, name: t.delete('#') } }
      Tag.create(tags)
    end
  end
end
