Taskitron::Application.routes.draw do
  devise_for :users
  devise_scope :user do
    get "sign_in", to: "devise/sessions#new"
    get "sign_up", to: "devise/registrations#new"
  end
  resources :tasks do
    member do
      post "stop", to: "tasks#stop"
      post "start", to: "tasks#start"
    end
  end
  resources :tags do
    collection do
      get '/:name', to: "tags#show", as: :name
      get '/:name/:day', to: 'tags#day', as: :day
    end
  end
  authenticated :user do
    root 'tasks#index', as: 'authenticated_root'
  end
  root 'welcome#index'
end
