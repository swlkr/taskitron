class TasksController < ApplicationController
  before_filter :authenticate_user!
  before_filter :assign_variables, only: [:index, :create]

  def index
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @task = Task.find(params[:id])
  end

  def create
    @task = current_user.tasks.build(task_params)
    if @task.save
      @task.start
      @task.parse_tags
      redirect_to authenticated_root_path
    else
      render :index
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy
    redirect_to authenticated_root_path
  end

  def start
    @task = Task.find(params[:id])
    @task.start
    redirect_to authenticated_root_path
  end

  def stop
    @task = Task.find(params[:id])
    @task.stop
    redirect_to authenticated_root_path
  end

  protected

  def assign_variables
    @tasks = current_user.tasks.reverse.page(params[:page])
    @task = Task.new
    @tags = current_user.popular_tags
  end

  def task_params
    params.require(:task).permit(:name)
  end
end
