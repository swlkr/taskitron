class TagsController < ApplicationController
  before_filter :find_tags_by_name, only: [:show, :day]
  def index
    @tags = current_user.popular_tags
  end

  def show
    @tasks = @tag.tasks
    @elapsed_time = @tag.elapsed_time
  end

  def day
    begin
      date = Time.parse(params[:day])
    rescue
      raise ActionController::RoutingError.new('Not Found') || not_found
    end
    @tasks = @tag.tasks.by_day(date) || not_found
    @elapsed_time = @tag.elapsed_time_by_day(date) || not_found
  end

  protected

  def find_tags_by_name
    @tag = current_user.tags.find_by_name(params[:name]) || not_found
  end
end