$(function() {
  var hashtags = false;

  $(document).on('keydown', '#new-task-content', function (e) {
    key = {
      hashtag: 51,
      space: 32
    };

    var content = $(this);

    switch (e.which) {
      case key.hashtag:
        if(e.shiftKey) {
          e.preventDefault();
          content.html(content.html() + "<span class='highlight'>#");
          placeCaretAtEnd(this);
          hashtags = true;
        }
        break;
      case key.space:
        if(hashtags) {
          e.preventDefault();
          content.html(content.html() + "</span>&nbsp;");
          placeCaretAtEnd(this);
          hashtags = false;
        }
        break;
    }
  });
});

function placeCaretAtEnd(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
          && typeof document.createRange != "undefined") {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
  } else if (typeof document.body.createTextRange != "undefined") {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
  }
}