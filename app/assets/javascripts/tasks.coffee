$(document)
  .on("input", "#new-task-content", ->
    $("#task_name").text $(this).text().replace(/\u00a0/g, ' ');
  )
  .on('change input', 'div[data-placeholder]', ->
    if this.textContent
      this.dataset.divPlaceholderContent = 'true'
    else
      delete this.dataset.divPlaceholderContent
  )