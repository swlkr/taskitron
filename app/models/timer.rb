class Timer < ActiveRecord::Base
  belongs_to :task, touch: true

  validates :task_id, presence: true

  scope :completed, -> { where("stopped_at IS NOT NULL") }

  def start
    self.started_at = Time.now
  end

  def stop
    self.stopped_at = Time.now
  end

  def started?
    started_at.present?
  end

  def stopped?
    stopped_at.present?
  end

  def completed?
    started? && stopped?
  end

  def elapsed_time
    completed? ? stopped_at - started_at : 0
  end
end
