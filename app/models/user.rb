class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tasks, dependent: :destroy

  def tags
    Tag.where(task_id: [tasks.pluck(:id)])
  end

  def popular_tags
    Tag.popular_by_user_id(id)
  end
end
