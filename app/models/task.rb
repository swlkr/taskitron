class Task < ActiveRecord::Base
  belongs_to :user
  has_many :timers, dependent: :destroy
  has_many :tags, dependent: :destroy

  validates :name, :user_id, presence: true
  validates :name, length: { in: 3..140 }

  scope :reverse, -> { order('created_at DESC') }
  scope :by_day, -> (date) { where(created_at: date.beginning_of_day..date.end_of_day) }

  self.per_page = 10

  def elapsed_time
    timers.completed.map(&:elapsed_time).inject { |time, sum| sum += time } || 0
  end

  def start
    timer.start
    timer.save
  end

  def stop
    current_timer = timer
    current_timer.stop
    current_timer.save
  end

  def completed?
    previous_timer = timers.last
    timer.new_record? && previous_timer.present?
  end

  def timer
    previous_timer = timers.last
    previous_timer.nil? || previous_timer.completed? ? timers.build : previous_timer
  end

  def parse_tags
    Tag.create(name.split('#').drop(1).map { |x| x[/\w+/] }.map { |tag| { task_id: id, name: tag.delete('#') } })
  end

  def hashtags
    tags.pluck(:name).map { |t| "##{t}" }
  end
end
