class Tag < ActiveRecord::Base
  belongs_to :task

  validates :name, :task_id, presence: true

  def days
    tasks.group_by { |t| t.created_at.to_date }.map { |date, tasks| date }.uniq
  end

  def tasks
    Task.joins(:tags).where('tags.name = ?', name)
  end

  def calculate_elapsed_time(tasks)
    tasks.map(&:elapsed_time).inject { |sum, t| sum += t } || 0
  end

  def elapsed_time
    calculate_elapsed_time(tasks)
  end

  def elapsed_time_by_day(date)
    calculate_elapsed_time(tasks.by_day(date))
  end

  def self.popular_by_user_id(user_id)
    Tag.joins(:task).where('tasks.user_id = ?', user_id).group('tags.name').order('count_name DESC').count('name')
  end
end
