module TasksHelper
  def format_elapsed_time(seconds)
    seconds = seconds.to_i
    days = seconds / 86400
    seconds -= days * 86400
    hours = seconds / 3600
    seconds -= hours * 3600
    minutes = seconds / 60
    seconds -= minutes * 60
    "#{days > 0 ? "#{pluralize(days, 'day')}," : ""} #{hours > 0 || days > 0 ? "#{pluralize(hours, 'hour')}," : ""} #{minutes > 0 || hours > 0 || days > 0 ? "#{pluralize(minutes, 'minute')}," : ""} #{seconds > 0 || minutes > 0 || hours > 0 || days > 0 ? pluralize(seconds, 'second') : "0 seconds"}".strip
  end

  def style_name(task)
    task.name.split.map { |s| task.hashtags.include?(s) ? content_tag(:a, href: "/tags/#{s.delete('#')}") { s } : s }.join(' ').html_safe
  end
end
